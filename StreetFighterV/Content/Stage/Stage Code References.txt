Stage Code References
=====================
AFB -------- Air Force Base (USA)
BOS -------- Lair of the Four Kings (???)
BRZ -------- Hillside Plaza (Brazil)
CHN -------- Bustling Side Street (China)
DBI -------- Skies of Honor (UAE)
DOJ -------- Dojo (???)
IND -------- Apprentice Alley (India)
KPB -------- Kanzuki Beach (Malaysia)
KZK -------- Kanzuki Estate (Japan)
KZ2 -------- Estate At Noon (Japan)
LDN -------- Union Station (England)
LVS -------- High Roller Casino (USA)
NYK -------- City in Chaos (USA)
NZL -------- Forgotten Waterfall (New Zealand)
NZ1 -------- Mysterious Cove (New Zealand)
RUS -------- Underground Arena (Russia)
RU2 -------- Spooky Arena (Russia)
S01 -------- Temple of Ascension (Japan)
S02 -------- Temple Hideout (Thailand)
S03 -------- English Manor (England)
S04 -------- Ring of Pride (Japan)
S05 -------- Frosty Boulevard (USA)
S06 -------- Kasugano Residence (Japan)
S07 -------- Metro City Bay Area (USA)
S08 -------- King's Court (Thailand)
S09 -------- Ring of Power (???)
S14 -------- Ring of Justice (USA)
S16 -------- Field of Fate (Japan)
SHA -------- Shadaloo Base (???)
SH4 -------- Shadaloo Base At Night (???)
SPN/SX1 -------- Flamenco Tavern (Spain)
SZK/SX2 -------- Suzaku Castle (Japan)
TAI -------- Ring of Destiny (USA)
TRN -------- The Grid (???)
